import os

dir = "docs/"


class BoolSearch(object):

    index = list()
    count_files = 0

    def __init__(self):
        self.count_files = len(os.listdir(dir))

    def incident_matrix(self):
        pass

    def inverted_index(self):
        """

        Build inverted index of files in directory 'dir'

        :return: List with inverted index
        """
        for i in range(self.count_files):
            file = open(dir + str(i + 1) + ".txt")
            words = file.read().split()

            for word in words:
                self.index.append([[word], [i + 1]])
        self.index = sorted(self.index, key=lambda s: s[0])

        i_new = 0
        count = 0
        new_index = []
        for i in range(len(self.index)):
            if count == 0:
                new_index.append(self.index[i])
            else:
                count -= 1
                continue
            for j in range(i + 1, len(self.index)):

                if self.index[i][0] != self.index[j][0]:
                    new_index[i_new][0].append(len(new_index[i_new][1]))
                    break

                new_index[i_new][1].append(self.index[j][1][0])
                count += 1
            i_new += 1
        self.index = new_index
        return self.index

    def search_request(self, query):
        request = query.split()
        p = []

        self.inverted_index()
        while request:
            word = request.pop()
            for pair in self.index:
                if pair[0][0] == word:
                    p.append(pair[1])
        return BoolSearch.intersect(p[0], p[1])

    @staticmethod
    def intersect(p1, p2):
        answer = []
        i, j = 0, 0
        while i < len(p1) and j < len(p2):
            if p1[i] == p2[j]:
                answer.append(p1[i])
                i += 1
                j += 1
            elif p1[i] < p2[i]:
                i += 1
            else:
                j += 1
        return answer

