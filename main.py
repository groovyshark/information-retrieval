import bool_search as bs

bs_var = bs.BoolSearch()

while True:
    query = input("Input request: ")
    if len(query) == 0:
        break
    print(bs_var.search_request(query))
